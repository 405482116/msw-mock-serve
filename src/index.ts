import { createMiddleware } from "@mswjs/http-middleware";
import { createHandlers } from "msw-dynamic-http-middleware";
import express from "express";
import cors from "cors";
import path from "path";
import { handlers } from "./handlers";
import { userScenarios } from "./scenarios";

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded());
app.use(express.static("client/build"));

const userHandlers = createHandlers(userScenarios, "success");

const middleware = createMiddleware(...handlers);
const userMiddleware = createMiddleware(...userHandlers);

app.use(middleware);

app.use(userMiddleware);

app.get("/ui", (_req, res) => {
  res.sendFile(path.join(process.cwd(), "client", "build", "index.html"));
});
app.listen(9080),
  () => {
    console.log(`Server started :: 9090`);
  };
