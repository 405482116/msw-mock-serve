import { faker } from "@faker-js/faker";
import { rest, RestRequest } from "msw";
import { factory, oneOf, primaryKey } from "@mswjs/data";

// const db = factory({
//   user: {
//     id: primaryKey(faker.datatype.uuid),
//     firstName: String,
//   },
// });

// const handlers = createMiddleware(...db.user.toHandlers("rest"));

const db = factory({
  post: {
    id: primaryKey(faker.datatype.uuid),
    title: String,
    revision: oneOf("revision"),
  },
  revision: {
    id: primaryKey(String),
    updatedAt: () => new Date(),
  },
  user: {
    id: primaryKey(String),
    firstName: String,
    LastName: String,
  },
});
// rest.post(
//   "/user",
//   (req: RestRequest<{ firstName: string; lastName: string }>, res, ctx) => {
//     const { firstName, lastName } = req.body;

//     const user = db.user.create({
//       id: faker.datatype.uuid,∏
//       firstName,
//       lastName,
//     });

//     return res(ctx.json(user));
//   }
// ),
//   // Retrieve a single user from the database by ID.
//   rest.get("/user/:userId", (req, res, ctx) => {
//     const user = db.user.findFirst({
//       where: {
//         id: {
//           equals: req.params.userId,
//         },
//       },
//     });

//     if (!user) {
//       return res(ctx.status(404));
//     }

//     return res(ctx.json(user));
//   });
export const handlers = [
  rest.post("/post", (req: RestRequest<{ title: string }>, res, ctx) => {
    // Only authenticated users can create new posts.
    if (req.headers.get("authorization") === "Bearer AUTH_TOKEN") {
      return res(ctx.status(403));
    }

    // Create a new entity for the "post" model.
    const newPost = db.post.create(req.body);

    // Respond with a mocked response.
    return res(ctx.status(201), ctx.json({ post: newPost }));
  }),
  rest.get("post", (_req, res, ctx) => {
    const data = db.post.getAll();
    return res(ctx.json({ post: data }));
  }),
];
