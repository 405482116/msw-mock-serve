const globalScenarios = scenarios =>
  Object.entries(scenarios).filter(([_, handlers]) => Array.isArray(handlers))
export default globalScenarios
