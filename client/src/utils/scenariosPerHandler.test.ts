import { scenariosPerHandler } from '.'

describe('transformData', () => {
  const data = {
    'user success': {
      isActive: false,
      header: 'GET /user',
      method: 'GET',
      path: '/user'
    },
    'user error': {
      isActive: false,
      header: 'GET /user',
      method: 'GET',
      path: '/user'
    },
    'users success': {
      isActive: false,
      header: 'GET /users',
      method: 'GET',
      path: '/users'
    },
    'users error': {
      isActive: false,
      header: 'GET /users',
      method: 'GET',
      path: '/users'
    },
    success: [
      {
        isActive: true,
        header: 'GET /user',
        method: 'GET',
        path: '/user'
      },
      {
        isActive: true,
        header: 'GET /users',
        method: 'GET',
        path: '/users'
      }
    ],
    error: [
      {
        isActive: false,
        header: 'GET /user',
        method: 'GET',
        path: '/user'
      },
      {
        isActive: false,
        header: 'GET /users',
        method: 'GET',
        path: '/users'
      }
    ]
  }
  it('works fine', () => {
    const result = scenariosPerHandler(data)
    console.log('🚀 ~ file: transformData.test.ts ~ line 60 ~ it ~ result', JSON.stringify(result))
  })
})
