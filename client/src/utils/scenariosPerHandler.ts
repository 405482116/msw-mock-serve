type RestHandlerInfo = { isActive: boolean; header: string; method: string; path: string }

const scenariosPerHandler = _scenarios =>
  Object.entries(_scenarios)
    .filter(([_, handlers]) => !Array.isArray(handlers))
    // Group by endpoint (info.header)
    .reduce((acc, scenarios) => {
      const [scenarioName, info] = scenarios as [string, RestHandlerInfo]
      const key = info.header
      const [method, path] = key.split(' ')
      if (!(key in acc)) {
        acc[key] = { method, path, scenarios: [] }
      }
      acc[key].scenarios.push({ active: info.isActive, label: scenarioName })

      return acc
      // eslint-disable-next-line max-len
    }, {} as Record<string, { method: string; path: string; scenarios: { active: boolean; label: string }[] }>)

export default scenariosPerHandler
