import { Dashboard } from 'components'
import React from 'react'
import { createRoot } from 'react-dom/client'

const container = document.getElementById('root')

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const root = createRoot(container!)

const App = () => {
  return <Dashboard />
}

root.render(<App />)
