import * as React from 'react'
import { makeStyles } from '@material-ui/styles'
import { createTheme } from '@material-ui/core/styles'

import { SettingsPanel } from 'components'
import Scenario from './Scenario'

const defaultTheme = createTheme()

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'flex',
      flexDirection: 'column',
      height: 600,
      width: '100%',
      '& .MuiFormGroup-options': {
        alignItems: 'center',
        paddingBottom: theme.spacing(1),
        '& > div': {
          minWidth: 100,
          margin: theme.spacing(2, 2, 2, 0)
        }
      }
    }
  }),
  { defaultTheme }
)

// SettingsPanel.propTypes = {
//   onApply: PropTypes.func.isRequired,
//   size: PropTypes.number.isRequired,
//   theme: PropTypes.oneOf(['ant', 'default']).isRequired,
//   type: PropTypes.oneOf(['Commodity', 'Employee']).isRequired
// }

const FullFeaturedDemo = () => {
  const classes = useStyles()
  const [type, setType] = React.useState('Commodity')
  const [size, setSize] = React.useState(100)
  const [pagination, setPagination] = React.useState({
    pagination: false,
    autoPageSize: false,
    pageSize: undefined
  })

  const handleApplyClick = settings => {
    if (size !== settings.size) {
      setSize(settings.size)
    }

    if (type !== settings.type) {
      setType(settings.type)
    }

    // if (size !== settings.size || type !== settings.type) {
    //   setRowLength(settings.size);
    //   loadNewData();
    // }

    const newPaginationSettings = {
      pagination: settings.pagesize !== -1,
      autoPageSize: settings.pagesize === 0,
      pageSize: settings.pagesize > 0 ? settings.pagesize : undefined
    }

    setPagination(currentPaginationSettings => {
      if (
        currentPaginationSettings.pagination === newPaginationSettings.pagination &&
        currentPaginationSettings.autoPageSize === newPaginationSettings.autoPageSize &&
        currentPaginationSettings.pageSize === newPaginationSettings.pageSize
      ) {
        return currentPaginationSettings
      }
      return newPaginationSettings
    })
  }

  return (
    <div className={classes.root}>
      <SettingsPanel onApply={handleApplyClick} size={size} type={type} />
      <Scenario />
    </div>
  )
}
export default FullFeaturedDemo
