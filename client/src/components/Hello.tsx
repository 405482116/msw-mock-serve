import * as React from 'react'

interface IHelloProps {
  title: string
}

const Hello = ({ title }: IHelloProps) => {
  return <h2>Hello {title}</h2>
}

export default Hello
