import React, { useEffect, useState } from 'react'
import { DataGrid, GridColDef, GridRenderCellParams, GridRowsProp } from '@mui/x-data-grid'
import { Api } from 'service'
import { scenariosPerHandler } from 'utils'
import Button from '@mui/material/Button/Button'

const Scenario = () => {
  const [scenario, setScenario] = useState({})

  useEffect(() => {
    const fetchScenario = async () => {
      try {
        const data = await Api<any>('/scenario')
        const _scenariosPerHandler = Object.values(scenariosPerHandler(data.scenarios))
        setScenario(data)
        console.log(
          '🚀 ~ file: Scenario.tsx ~ line 11 ~ fetchScenario ~ data',
          _scenariosPerHandler
        )
      } catch (error) {
        console.log('🚀 ~ file: Scenario.tsx ~ line 11 ~ fetchScenario ~ error', error)
      }
    }
    fetchScenario()
  }, [])
  const handlerOnClick = async ({ label, isActive }: { label: string; isActive?: boolean }) => {
    console.log('🚀 ~ file: Scenario.tsx ~ line 27 ~ handlerOnClick ~ isActive', isActive)
    console.log('🚀 ~ file: Scenario.tsx ~ line 27 ~ handlerOnClick ~ label', label)
  }
  const rows: GridRowsProp = [
    {
      id: 1,
      method: 'GET',
      path: '/user',
      scenarios: [
        {
          active: false,
          label: 'user success'
        },
        {
          active: false,
          label: 'user error'
        }
      ]
    },
    {
      id: 2,
      method: 'GET',
      path: '/users',
      scenarios: [
        {
          active: false,
          label: 'users success'
        },
        {
          active: false,
          label: 'users error'
        }
      ]
    }
  ]
  const columns: GridColDef[] = [
    { field: 'method', headerName: 'Method', width: 150 },
    { field: 'path', headerName: 'Path', width: 150 },
    {
      field: 'scenarios',
      headerName: 'Scenario',
      flex: 1,
      minWidth: 150,
      sortable: false,
      renderCell: (params: GridRenderCellParams) => {
        return (
          <strong>
            {params.value.map(item => {
              return (
                <Button
                  key={item.label}
                  variant="contained"
                  color="primary"
                  size="small"
                  onClick={() => handlerOnClick(item)}
                  style={{ marginLeft: 16 }}
                >
                  {item.label}
                </Button>
              )
            })}
          </strong>
        )
      }
    }
  ]

  return (
    <div style={{ display: 'flex', height: '100%' }}>
      <div style={{ flexGrow: 1 }}>
        <DataGrid rows={rows} columns={columns} />
      </div>
    </div>
  )
}

export default Scenario
