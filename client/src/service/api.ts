function api<T>(url: string, config: RequestInit = {}): Promise<T> {
  return (
    fetch(url, config)
      // When got a response call a `json` method on it
      .then(response => response.json())
      // and return the result data.
      .then(data => data as T)
  )
}

export default api
